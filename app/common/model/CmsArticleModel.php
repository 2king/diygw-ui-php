<?php

namespace app\common\model;

use diygw\model\DiygwModel;

/**
 * Class CategoryModel
 * @package app\common\model
 * @auth diygw
 */
class CmsArticleModel extends DiygwModel
{
    // 表名
    public $name = 'cms_article';
}